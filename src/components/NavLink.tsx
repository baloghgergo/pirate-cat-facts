import { Link, useColorModeValue } from "@chakra-ui/react";
import { NavLink as RouterLink } from "react-router-dom";
import splitbee from "@splitbee/web";
import type {ReactNode} from 'react';
interface NavLinkProps {
    to: string,
    children: ReactNode
}
const NavLink: React.FC<NavLinkProps> = ({  to, children }) => {
  const handleClick = () => {
    splitbee.track("Link Clicked", { to });
  };
  return (
    <Link
      as={RouterLink}
      textDecoration={"underline"}
      letterSpacing={"0.3px"}
      fontSize={"20px"}
      _hover={{
        textDecoration: "underline",
        color: useColorModeValue("brand.lightBlue", "brand.red"),
      }}
      to={to}
      onClick={handleClick}
    >
      {children}
    </Link>
  );
};
export default NavLink;
