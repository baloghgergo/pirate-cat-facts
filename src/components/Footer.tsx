import {
  Box,
  Container,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import  ExternalLink  from "./ExternalLink";
export default function Footer() {
  return (
    <Box color={useColorModeValue("gray.700", "gray.200")} mt={20}>
      <Container>
        <ExternalLink href={"https://gergobalogh.me"}>
          <Text textAlign="center">Made by Gergo Balogh</Text>
        </ExternalLink>
      </Container>
    </Box>
  );
}
