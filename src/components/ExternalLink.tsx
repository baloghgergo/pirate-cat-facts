import { Link, useColorModeValue } from "@chakra-ui/react";
import splitbee from "@splitbee/web";
import type {ReactNode} from 'react';
interface ExternalLinkProps {
    href: string,
    children: ReactNode
}
const ExternalLink: React.FC<ExternalLinkProps> = ({ href, children }) => {
  const handleClick = () => {
    splitbee.track("External link clicked", { href });
  };
  return (
    <Link
      textDecoration={"none"}
      fontStyle={"italic"}
      letterSpacing={"0.3px"}
      fontSize={"1xl"}
      _hover={{
        textDecoration: "underline",
        color: useColorModeValue("brand.lightBlue", "brand.red"),
      }}
      href={href}
      target="_blank"
      onClick={handleClick}
    >
      {children}
    </Link>
  );
};
export default ExternalLink;