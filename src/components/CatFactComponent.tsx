import {
    Button, Fade,
    VStack,
    GridItem, Img, Skeleton, Text, Box, Center, SkeletonText, useBreakpointValue
} from "@chakra-ui/react";
import {useEffect, useState} from "react";
import {GiPirateFlag} from "react-icons/gi";
import spaceman from "../assets/pirate_in_space.png";
import splitbee from '@splitbee/web';
type CatFactData = {
    catFact: string;
    catImageUrl: string;
};
export function CatFactComponent() {
    const [isLoaded, setIsLoaded] = useState<Boolean>(false);
    const [data, setData] = useState<CatFactData>({catFact: '', catImageUrl: ''});
    const isMobile = useBreakpointValue({ base: true, md: false });

    const fetchData = async () => {
        setData({catFact: '', catImageUrl: ''});
        setIsLoaded(false)
        try {
            let catFact = '';
             await fetch('https://api.val.town/v1/run/politelyinvinciblepointer.catFact')
            .then((response) => {
                response.json().then((result) => {
                    let story = atob(result.body);
                    let jsonData = JSON.parse(story)
                    catFact = jsonData.story
                })
            });
            let catImageUrl = ''
            await fetch('https://api.thecatapi.com/v1/images/search?size=full')
                .then((response) => {
                    if (response.status === 200) {
                        return response.json();
                    } else {
                        console.log('Error:', response.statusText);
                        throw new Error('Failed to fetch data');
                    }
                })
                .then((data) => {
                    catImageUrl = data[0].url
                })
                .catch((error) => {
                    console.error(error);
                });
            setData({
                catFact,
                catImageUrl
            })
            setIsLoaded(true);
        } catch (error) {
            console.error('Error:', error);
            setData({
                catFact: '"Arrr, we\'ve hit a rough patch on the digital seas!',
                catImageUrl: spaceman
            });
        }
    };
    useEffect(() => {
        fetchData();
    }, []);
    const handleButtonClick = () => {
        splitbee.track('Yarr button clicked')
        fetchData();
    }
    return (
        <VStack>
            <Box>
                    <Skeleton isLoaded={isLoaded ? true : false} w={isMobile ? 200: 400} h={isMobile ? 200: 400}>
                        <Center>
                            <Img src={data.catImageUrl} rounded="md" maxW={isMobile ? 200: 400} maxH={isMobile ? 200: 400}/>
                        </Center>
                    </Skeleton>
            </Box>
            <Box mt={5} maxW="container.md">
                <SkeletonText isLoaded={isLoaded ? true : false} noOfLines={2} spacing='4' skeletonHeight='2'>
                 <Fade
                    in={data ? true : false}
                    transition={{enter: {duration: 4}}}
                >
                    <Text textAlign="center" fontSize="2xl">{data.catFact}
                    </Text>
                </Fade>
                </SkeletonText>
            </Box>
            <GridItem display="flex" alignItems="center">
                <Button
                    size='lg'
                    isLoading={!isLoaded}
                    leftIcon={<GiPirateFlag/>}
                    onClick={handleButtonClick}
                    loadingText="Sendin' it off to Davy Jones' locker!"
                    spinnerPlacement='end'
                >"Yarr, hand over more!</Button>
            </GridItem>
        </VStack>
    );
}

