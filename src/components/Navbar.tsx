import {
  Box,
  Button,
  Flex,
  HStack,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import {MoonIcon, SunIcon } from "@chakra-ui/icons";

export default function Navbar() {
  const { colorMode, toggleColorMode } = useColorMode();
  const handleClick = () => {
    toggleColorMode();
  };

  return (
    <>
      <Box px={4} mt={{ lg: "15px" }}>
        <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
          <Flex />
          <Flex alignItems={"center"}>
            <HStack
              as={"nav"}
              spacing={4}
              display={{ base: "none", md: "flex" }}
              marginRight={5}
            />
            <Button
                size='lg'
              onClick={handleClick}
              bg={useColorModeValue("blackAlpha.100", "whiteAlpha.100")}
            >
              {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            </Button>
          </Flex>
        </Flex>
      </Box>
    </>
  );
}
