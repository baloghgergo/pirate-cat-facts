import { Button, Center, Fade, Flex, Heading, Box, Skeleton, Img, useBreakpointValue} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { CatFactComponent } from "../components/CatFactComponent";
import { GiPirateFlag } from "react-icons/gi";
import cannon_fire from '../assets/cannon_fire.mp3';
import pirate_with_cat from '../assets/pirate_with_cat.png';
import splitbee from '@splitbee/web';

const CatFactPage = () => {
  const [isFadedIn, setIsFadedIn] = useState(false);
  const [fadeInButton, setFadeInButton] = useState(false);
  const [fadeInCatFact, setFadeInCatFact] = useState(false);
  const [playSound, setPlaySound] = useState(false);
  const [isLoaded, setIsloaded] = useState(false);
  const isMobile = useBreakpointValue({ base: true, md: false });

  const toggleStartCatFacts = () => {
    splitbee.track('Start game')
    if(!playSound) {
      const audio = new Audio(cannon_fire);
      audio.play();
      setPlaySound(true);
    }
    setFadeInCatFact(true);
  };
  useEffect(() => {
    setIsloaded(true)
    const timeout = setTimeout(() => {
      setIsFadedIn(true);
    }, 1000);
    const timeoutButton = setTimeout(() => {
      setFadeInButton(true);
    }, 2000);
    return () => {
      clearTimeout(timeout);
      clearTimeout(timeoutButton);
    };
  }, [isFadedIn]);

  return (
    <Center>
      <Flex direction="column" align="center" px={4}>
        {fadeInCatFact ? null :
            <Box>
              <Skeleton isLoaded={isLoaded} w={isMobile ? 200: 400} h={isMobile ? 200: 400}>
                <Center>
                  <Img src={pirate_with_cat} rounded="md"  maxW={isMobile ? 200: 400} maxH={isMobile ? 200: 400}/>
                </Center>
              </Skeleton>
            </Box>
        }
        {fadeInCatFact ? (
          <>
            {" "}
            <Fade
              in={fadeInCatFact}
              transition={{ exit: { delay: 1 }, enter: { duration: 4 } }}
            >
              <CatFactComponent />
            </Fade>
          </>
        ) : (
            <Box mt={5}>
              <Fade
                  in={isFadedIn}
                  transition={{ exit: { delay: 1 }, enter: { duration: 4 } }}
              >
                <Heading>Arrr, ye be seekin' some fine cat facts?
                </Heading>
              </Fade>
              <Fade
                  in={fadeInButton}
                  transition={{ exit: { delay: 3 }, enter: { duration: 4 } }}
              >
                <Center>
                  <Button size='lg' leftIcon={<GiPirateFlag />} onClick={toggleStartCatFacts}>Aye, aye, Captain!</Button>
                </Center>
              </Fade>
            </Box>
        )}
      </Flex>
    </Center>
  );
};
export default CatFactPage;
