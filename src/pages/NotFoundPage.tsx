import { Container, Flex, Heading, Img } from "@chakra-ui/react";
import spaceman from "../assets/pirate_in_space.png";
import NavLink from "../components/NavLink";
const NotFoundPage = () => {
  return (
    <Container>
      <Img src={spaceman} borderRadius="5px" />
      <Heading
        textAlign="center"
        fontSize={{ base: "3xl", md: "4xl", lg: "6xl" }}
        fontWeight="bold"
        lineHeight={{ base: "34px", md: "48px", lg: "72px" }}
      >
          We've stumbled upon the dreaded 404, where there be nothin' but empty waters!
      </Heading>
      <Flex justifyContent="center" alignItems="center">
        <NavLink to={"/"}>
          Go back
        </NavLink>
      </Flex>
    </Container>
  );
};

export default NotFoundPage;
