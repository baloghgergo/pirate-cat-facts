import './App.css'
import { ChakraProvider, Container, extendTheme} from '@chakra-ui/react'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "@fontsource/eb-garamond/500-italic.css";
import "@fontsource/eb-garamond/";
import ScrollToTop from "./components/ScrollToTop";
import Navbar from "./components/Navbar";
import CatFactPage from "./pages/CatFactPage";
import NotFoundPage from "./pages/NotFoundPage";
import Footer from "./components/Footer";
import splitbee from '@splitbee/web';
const theme = extendTheme(
    {
        colors: {
            brand: {
                900: "#1a365d",
                800: "#153e75",
                700: "#2a69ac",
                brown: "#C7C3B4",
                blue: "#599EB4",
                lightBlue: "#16A1AD",
                red: "#F55929",
                darkBlue: "#1a6f80",
                background: "#FFF4F4",
                lightBrown: "#ededee",
            },
        },
        initialColorMode: "light",
        useSystemColorMode: false,
        fonts: {
            heading: `'EB garamond', serif`,
            body: `'EB garamond', serif`,
        },
    },

);
function App() {
    splitbee.init()
  return (
      <ChakraProvider theme={theme}>
          <Router>
              <Container maxW="container.xl">
                  <Navbar />
              </Container>
              <ScrollToTop />
              <Container maxW="container.xl">
                  <Routes>
                      <Route path="/"  element={<CatFactPage />} />
                      <Route path="*" element={<NotFoundPage />} />
                  </Routes>
              </Container>
              <Footer />
          </Router>
      </ChakraProvider>
  )
}

export default App
