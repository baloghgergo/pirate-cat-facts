# Cat Facts to Pirate Slang Transformer

Transform ordinary cat facts into swashbuckling pirate slang.
You also get a free kit pic!

## Getting Started

These instructions will help you set up and run the project on your local machine.

### Prerequisites

Before you begin, make sure you have the following installed on your system:

- Node.js: [Download and Install Node.js](https://nodejs.org/)

### Installation
Install project dependencies:
```bash
npm install
```

### Usage
To start the development server, run the following command:
```bash
npm run dev
```

###  How It Works
Click the "Yarr, hand over more!" button to fetch a new cat fact in pirate slang.
A cat image and a transformed cat fact will be displayed.
### Built With
- React
- Vite
- TypeScript
- Chakra UI
- OpenAI API

### Acknowledgments
>Cat facts provided by [The CatFact API](https://catfact.ninja/).
> 
>Pirate slang transformation powered by OpenAI's GPT-3.5 Turbo.
> 
>Special thanks to [Val town](https://www.val.town/) for providing simple, free and serverless backends! 
###License
This project is licensed under the MIT License - see the LICENSE file for details.